import GAME as GAME
from django.db import models

# Create your models here.
from django.db.models import CharField, ForeignKey

from django.urls import reverse
from django.db import models


class Video(models.Model):
    name = models.CharField(max_length=255, verbose_name="Название ролика")
    describtion = models.TextField(verbose_name="Описание ролика", blank=True)
    videfile = models.FileField(verbose_name="Файл видео")
    created_datetime = models.DateTimeField(auto_now_add=True)

    GAME_CATEGORY = "Game"
    FILM_CATEGORY = "Film"
    CATS_CATEGORY = "Cats"
    category = models-CharField(max_length=10,
                                verbose_name="Категория ролика",
                                choices=[
                                    (GAME, "Игры"),
                                    (FILM_CATEGORY, "Фильмы"),
                                    (CATS_CATEGORY, "Коты"),
                                ],
                                )
    NO_RESTRICTION = "NO"
    VIOLATION_RESTRICTION = "VIOLATION"
    SHADOW_BAN_RESTRICTION = "Shadow"
    BAN_RESTRICTION = "BAN"
    restrictions = models.CharField(max_length=10,
                                    verbose_name="Ограничение",
                                    choices=[
                                        (NO_RESTRICTION, "Нет ограничений"),
                                        (VIOLATION_RESTRICTION, "Нарушение"),
                                        (BAN_RESTRICTION, "Бан"),
                                        (SHADOW_BAN_RESTRICTION, "Теневой бан"),
                                    ],
                                        default=NO_RESTRICTION,
                                    )
    created_by = models.ForeignKey(
        "auth.User", verbose_name="Автор", on_delete=models.SET_NULL, null=True)

    def ＿str＿(self) -> str:
        return f"{self.id}|{self.name} {{self.description[0:20}}…)"

    @property
    def get_absolute_url (self):
        return reverse("video", kwargs={'id': self.id})

    @property
    def likes(self):
        return self.like_set.filter(value＿gt=0).count()

    @property
    def dislikes(self):
        return self.like_set.filter(value＿lt=0).count()

    class Meta:
        verbose_name = "Видео-ролик"
        verbose_name_plural = "Видео-ролики"

class Like(models.Model):
    video_id = models-ForeignKey(Video, verbose_name="Видео",
                                on_delete=models.SET_NULL,
                                null=True)
    user_id = models.ForeignKey("auth.User",
                            on_delete=models.SET_NULL,
                            null=True)
    value = models.IntegerField(verbose_name="Лайк(1)/Дизлайк(-1)", default=0)

    class Meta:
        verbose_name = "Лайк/Дизлайк"
        verbose_name_plural = "Лайки/Дизлайки"

class Comment(models.Model):
    text = models.TextField(verbose_name="Текст комментария")
    user = models.ManyToManyField("auth.User")
    video_id = models.ForeignKey(Video,
                                on_delete=models.SET_NULL,
                                null=True)
    created_datetime = models.DateTimeField(
        verbose_name="Время создание", auto_now_add=True)

    class Meta:
        verbose_name = "Коментарий"
        verbose_name_plural = "Коментарии"

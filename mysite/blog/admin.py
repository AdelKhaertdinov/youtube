from django.contrib import admin

# Register your models here.

from .models import(
    Video,
    Like,
    Comment
)

admin.site.register([
    Video,
    Like,
    Comment,
])